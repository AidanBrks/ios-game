//
//  GameScene.swift
//  BrookAidanMobileGame
//
//  Created by BROOKS, AIDAN on 12/11/2019.
//  Copyright © 2019 BROOKS, AIDAN. All rights reserved.
//
import SpriteKit
import GameplayKit
import CoreMotion // Needed for MotionManager

class GameScene: SKScene, SKPhysicsContactDelegate {
    
    /*Sprites*/
    var player = SKSpriteNode()
    // Array of the Possible Sprites to use as enemy
    var arrayofhostiles = ["meteor01","meteor02","fireball01"]
    var background = SKSpriteNode()
    var bullet = SKSpriteNode()
    var ground = SKSpriteNode()
    var bulletHitMeteorParticle = SKEmitterNode()
    /*Misc*/
    var GameTimer = Timer()
    //Allows us to check and Store User Settings
    let defaults = UserDefaults.standard
    /*Score*/
    var ScoreText = SKLabelNode()
    var HasEarthBeenHit:Int = 0
    var Score: Int = 0 {
        didSet
        {
            ScoreText.text = "Score: \(Score)"
        }
    }
    var HighScore:Int = 0;
    
    /* UIGestureRecognizers + Accelermoter Values*/
    let motion = CMMotionManager()
    let swipeUpRec = UISwipeGestureRecognizer()
    let tapRec = UITapGestureRecognizer()
    let tapRec2 = UITapGestureRecognizer()
    var destX:CGFloat = 0.0
    override func didMove(to view: SKView) {
        let HighScore = defaults.integer(forKey: "HighScore")
        
        //Setup Physics
        self.physicsWorld.contactDelegate = self
        //SetUp Sprites + Force Origin Point
        self.anchorPoint = CGPoint(x: 0, y: 0)
        SetupSprites()
        //Timer - Required to spawn the Hostiles overtime (repeating)
        GameTimer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(CreateHostiles), userInfo: nil, repeats: true)
        
        /* Gestures and Accelerometer */
        tapRec.addTarget(self, action:#selector(GameScene.tappedView(_:)))
        tapRec.numberOfTouchesRequired = 1
        tapRec.numberOfTapsRequired = 1
        self.view!.addGestureRecognizer(tapRec)
        
        swipeUpRec.addTarget(self, action: #selector(GameScene.swipedUp))
        swipeUpRec.direction = .up
        self.view!.addGestureRecognizer(swipeUpRec)
        
        if motion.isAccelerometerAvailable
        {
            motion.accelerometerUpdateInterval = 0.01
            motion.startAccelerometerUpdates(to: .main) {
                (data,error) in
                guard let data = data , error == nil else {
                    return
                }
                let currentX = self.player.position.x
                self.destX = currentX + CGFloat(data.acceleration.x * 1200)
            }
        }
    }
    
    
    /* Functions to make nodes */
    
    func CreatePlayer()
    {
        //Player Sprite Setup
        player = SKSpriteNode(imageNamed: "player")
        player.position = CGPoint(x: self.frame.size.width / 2, y: player.size.height / 2 + 120)
        player.zPosition = 1
        // Player Physics Setup
        player.physicsBody = SKPhysicsBody(texture: player.texture!, size: player.texture!.size())
        player.physicsBody?.categoryBitMask = PhysicsCategories.playerCater
        player.physicsBody?.usesPreciseCollisionDetection = true
        player.physicsBody?.isDynamic = true
        player.physicsBody?.collisionBitMask &= ~PhysicsCategories.hostileCater
        player.physicsBody?.collisionBitMask &= ~PhysicsCategories.bulletCater
        player.physicsBody?.allowsRotation = false
        
        //Player Contrainsts to Screen
        let width2 = player.size.width / 2
        let height2 = player.size.height / 2
        let yRange = SKRange(lowerLimit: 0 + height2, upperLimit: size.height - height2)
        let xRange = SKRange(lowerLimit: 0 + width2, upperLimit: size.width - width2)
        player.constraints = [SKConstraint.positionX(xRange, y: yRange)]
        addChild(player)
        
    }
    
    func CreateAndShootPlayerWeapon()
    {
        // Player Shoots Play Sound
        self.run(SKAction.playSoundFileNamed("soundbible_Shoot", waitForCompletion: false))
        bullet = SKSpriteNode(imageNamed: "bullet")
        bullet.position = player.position
        bullet.position.y += 30
        //Bullet Physics Setup
        bullet.physicsBody = SKPhysicsBody(circleOfRadius: bullet.size.width / 2)
        bullet.physicsBody?.isDynamic = true // Is moved by the Physics Simulation
        //Physics Category setup
        bullet.physicsBody?.categoryBitMask = PhysicsCategories.bulletCater
        // What PhysicsBody(Sprite) is Tested
        bullet.physicsBody?.contactTestBitMask = PhysicsCategories.hostileCater
        addChild(bullet)
        //Array of Actions to move sprite down
        var ArrayofActions = [SKAction]()
        ArrayofActions.append(SKAction.move(to: CGPoint(x: player.position.x ,y: self.frame.size.height + 10), duration: 0.3))
        ArrayofActions.append(SKAction.removeFromParent())
        bullet.run(SKAction.sequence(ArrayofActions))
    }
    
    @objc func CreateHostiles()
    {
        arrayofhostiles = GKRandomSource.sharedRandom().arrayByShufflingObjects(in: arrayofhostiles) as! [String]
        let hostiles = SKSpriteNode(imageNamed: arrayofhostiles[0])
        let randomSpawnPosition = GKRandomDistribution(lowestValue: 0, highestValue: 400)
        let hostilePosition = CGFloat(randomSpawnPosition.nextInt())
        hostiles.position = CGPoint(x: hostilePosition, y: self.frame.size.height + hostiles.size.height)
        //Physics Setup
        hostiles.physicsBody = SKPhysicsBody(rectangleOf: hostiles.size)
        hostiles.physicsBody?.isDynamic = true
        hostiles.physicsBody?.categoryBitMask = PhysicsCategories.hostileCater
        // What PhysicsBody(Sprite) is Tested
        hostiles.physicsBody?.contactTestBitMask = PhysicsCategories.bulletCater
        hostiles.physicsBody?.collisionBitMask = PhysicsCategories.bulletCater
        addChild(hostiles)
        
        //Array of Actions to move the sprite down
        var ArrayOfActions = [SKAction]()
        ArrayOfActions.append(SKAction.move(to: CGPoint(x: hostilePosition ,y: -hostiles.size.height),duration: 1))
        ArrayOfActions.append(SKAction.removeFromParent())
        hostiles.run(SKAction.sequence(ArrayOfActions))
        
        
    }
    func CreateBackground()
    {
        //Backgorund Setup
        background = SKSpriteNode(imageNamed:"background02")
        background.position = CGPoint(x: self.frame.midX, y: self.frame.midY)
        background.zPosition = -1
        //Scale the background to the screens size
        background.size = CGSize(width:self.frame.size.width, height: self.frame.size.height)
        addChild(background)
        //backgroundColor = SKColor.blue
    }
    func CreateGround()
    {
        //Ground Setup
        ground = SKSpriteNode(imageNamed: "floor01")
        ground.name = "Ground01"
        ground.position = CGPoint(x: self.frame.midX , y: ground.size.height / 2 + -80)
        ground.zPosition = 5
        //Physics Setup
        ground.physicsBody = SKPhysicsBody(texture: ground.texture!, size: ground.texture!.size())
        ground.physicsBody?.isDynamic = false
        //Bullet has Physics Category of bullet.
        ground.physicsBody?.categoryBitMask = PhysicsCategories.groundCater
        // What PhysicsBody(Sprite) is Tested
        ground.physicsBody?.collisionBitMask = PhysicsCategories.hostileCater
        ground.physicsBody?.contactTestBitMask = PhysicsCategories.hostileCater
        addChild(ground)
    }
    /* Create Score */
    func CreateScoreText()
    {
        //iosfont.com
        ScoreText = SKLabelNode(fontNamed: "Futura")
        ScoreText.text = "Score: 0"
        ScoreText.fontSize = 65
        ScoreText.fontColor = SKColor.black
        //ScoreText.horizontalAlignmentMode = .right
        //ScoreText.position = CGPoint(x: 280, y: 1210)
        ScoreText.position = CGPoint(x:self.frame.width / 2 , y: self.frame.height - 60)
        addChild(ScoreText)
    }
    /*Setup the Nodes required for the Game*/
    func SetupSprites()
    {
        CreateScoreText()
        CreatePlayer()
        CreateBackground()
        CreateGround()
    }
    /*
     * Gestures Functions and Taps Functions what get called
     */
    
    //One Tap
    @objc func tappedView(_ sender:UITapGestureRecognizer)
    {
        CreateAndShootPlayerWeapon()
        //Shoot Once
    }
    
    //SwipeUp Gesture
    @objc func swipedUp() {
        SKAction.playSoundFileNamed("soundbible_Jump", waitForCompletion: true)
        player.physicsBody?.velocity = self.physicsBody!.velocity
        player.physicsBody?.applyImpulse(CGVector(dx: 0, dy: 20))
    }
    
    /*
     * Gesture Removers (If Needed)
     */
    func removeUpGesture()
    {
        self.view?.removeGestureRecognizer(swipeUpRec)
    }
    
    func removeTapGesture()
    {
        self.view?.removeGestureRecognizer(tapRec)
    }
    
    //Two Physics Bodies have collided with each other
    func didBegin(_ contact: SKPhysicsContact) {
        let collision:UInt32 = contact.bodyA.categoryBitMask | contact.bodyB.categoryBitMask
        
        if collision == PhysicsCategories.groundCater | PhysicsCategories.hostileCater {
            HasEarthBeenHit += 1
            if (HasEarthBeenHit == 10)
            {
                if (Score > HighScore)
                {
                    defaults.set(Score, forKey:"HighScore")
                    
                    let sw_EndGameScene = SKScene(fileNamed: "EndGameScene")
                    self.view!.presentScene(sw_EndGameScene)
                }
                else {
                    defaults.set(Score, forKey:"Score")
                    //                    let sw_EndGameScene = MainMenuScene(size: self.size)
                    let sw_EndGameScene = SKScene(fileNamed: "EndGameScene")
                    self.view!.presentScene(sw_EndGameScene)
                }
            }
            SKAction.playSoundFileNamed("soundbible_Meteor.wav", waitForCompletion: true)
            contact.bodyB.node?.removeFromParent()
            
            
        }
        if collision == PhysicsCategories.bulletCater | PhysicsCategories.hostileCater {
            Score += 10
            if let bulletHitMeteorParticle = SKEmitterNode(fileNamed: "bullet_hit_Meteor.sks")
            {
                SKAction.playSoundFileNamed("soundbible_8bitExplosion", waitForCompletion: false)
                let bodyBCollidePoint:CGPoint = contact.contactPoint
                bulletHitMeteorParticle.position = bodyBCollidePoint
                addChild(bulletHitMeteorParticle)
            }
            
            //Remove the Bullet and Meteor/Fireball from Scene
            contact.bodyA.node?.removeFromParent()
            contact.bodyA.node?.removeAllActions()
            contact.bodyB.node?.removeFromParent()
            contact.bodyB.node?.removeAllActions()
            // SKAction Wait to Remove Emitter node
            let WaitTime:Double = 5
            run(SKAction.wait(forDuration: WaitTime))
            {
                self.bulletHitMeteorParticle.removeFromParent()
            }
        }
    }
    /*
     * Update
     */
    
    override func update(_ currentTime: TimeInterval) {
        //Player will move
        let moveplayeraction = SKAction.moveTo(x: destX, duration: 1)
        player.run(moveplayeraction)
        // Called before each frame is rendered
    }
}
