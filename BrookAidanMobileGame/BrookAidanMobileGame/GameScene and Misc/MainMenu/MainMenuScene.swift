//
//  MainMenuScene.swift
//  BrookAidanMobileGame
//
//  Created by BROOKS, AIDAN on 08/12/2019.
//  Copyright © 2019 BROOKS, AIDAN. All rights reserved.
//
import SpriteKit

class MainMenuScene: SKScene {
    
    var LoadGameLabel:SKLabelNode!
    var Logo:SKSpriteNode!
    
    override func didMove(to view: SKView)
    {
        self.size = CGSize(width: 750.0, height: 1334.0)
        /*
         * :)
         */
    }
    
    //Load the GameScene on SKLabelTouch.
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        let touch = touches.first
        if let location = touch?.location(in: self)
        {
            let node = self.nodes(at: location)
            
            if (node[0].name == "LoadGameLabel")
            {
                //let transition = SKTransition.moveIn(with:, duration: 0.1)
                //let transition = SKTransition.flipHorizontal(withDuration: 0.5)
                let sw_GameScene = GameScene(size: self.size)
                self.view!.presentScene(sw_GameScene)
            }
            else if(node[0].name != "LoadGameLabel") {
                // Do nothing
            }
        }
    }
    
}
