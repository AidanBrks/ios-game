//
//  EndScoreScene.swift
//  BrookAidanMobileGame
//
//  Created by BROOKS, AIDAN on 28/11/2019.
//  Copyright © 2019 BROOKS, AIDAN. All rights reserved.
//

import SpriteKit

class EndScoreScene: SKScene {
    var RePlayButton = SKLabelNode()

     override func didMove(to view: SKView) {
        //iosfont.com
        RePlayButton = SKLabelNode(fontNamed: "Futura")
        RePlayButton.text = "Click to Replay"
        RePlayButton.fontSize = 65
        RePlayButton.color = SKColor.white
        //RePlayButton.position = CGPoint(x: 280, y: 1210)
        RePlayButton.position = CGPoint(x:250, y: self.frame.height - 60)
        addChild(RePlayButton)

     }


}