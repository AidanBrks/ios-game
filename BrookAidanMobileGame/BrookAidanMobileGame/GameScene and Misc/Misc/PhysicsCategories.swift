//
//  PhysicsCategories.swift
//  BrookAidanMobileGame
//
//  Created by BROOKS, AIDAN on 03/12/2019.
//  Copyright © 2019 BROOKS, AIDAN. All rights reserved.
//

// PhysicsCategories Using Bitshifiting
import Foundation
// player,hostiles,background,bullet,ground
struct PhysicsCategories {
    static let playerCater : UInt32 = 0x1 << 0
    static let hostileCater : UInt32 = 0x1 << 1
    static let groundCater : UInt32 = 0x1 << 2
    static let bulletCater : UInt32 = 0x1 << 3
}
