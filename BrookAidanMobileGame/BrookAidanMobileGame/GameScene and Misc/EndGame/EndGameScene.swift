//
//  EndGameScene.swift
//  BrookAidanMobileGame
//
//  Created by BROOKS, AIDAN on 09/12/2019.
//  Copyright © 2019 BROOKS, AIDAN. All rights reserved.
//

import SpriteKit


class EndGameScene: SKScene {
    // Return Player to MainMenuScene
    var ReturnToMenu:SKLabelNode!
    // Devices HighScore
    var Title_HighScore:SKLabelNode!
    var HighScore:SKLabelNode!
    //Allows us to check and Store User Settings
    let defaults = UserDefaults.standard
    
    
    
    override func didMove(to view: SKView) {
        self.size = CGSize(width: 750.0, height: 1334.0)
        self.anchorPoint = CGPoint(x: 0.5, y: 0.5)
        
        //let Scoreint = defaults.integer(forKey: "Score")
        let HighScoreint = defaults.integer(forKey: "HighScore")
        
        // if let PlayerScore = self.childNode(withName: "PlayerScore") as? SKLabelNode
        //{
        //    PlayerScore.text = "\(Scoreint)"
        // }
        
        if let HighScore = self.childNode(withName: "HighScore") as? SKLabelNode
        {
            HighScore.text = "\(HighScoreint)"
        }
        
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        let touch = touches.first
        if let location = touch?.location(in: self)
        {
            let node = self.nodes(at: location)
            
            if node[0].name == "ReturnToMenu"
            {
                let sw_MainMenuScene = SKScene(fileNamed: "MainMenuScene")
                self.view!.presentScene(sw_MainMenuScene)
            }
        }
    }
    
    
    
    
}

